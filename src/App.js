import './App.css';
import {useEffect, useState} from "react";
import axios from "axios";
import Countries from "./Components/Countries";
import Selected from "./Container/Selected";


const App = () => {

  const [countries, setCountries] = useState([{
      name:'',
      alpha3Code: '',
      id: '1',
      borders: '',
      capital: ''
  }]);

  const [selectedCountry, setSelectedCountry] = useState([

  ]);

  useEffect(() => {

    const axiosData = async () => {

      try {
        const response = await axios.get('https://restcountries.eu/rest/v2/all?fields=name;alpha3Code');

        const count = response.data

        setCountries([...count]);
      } catch (e) {
        alert('Error : '+ e.response.status )
      }

    };
    axiosData().catch(e => console.error(e));
  });

  const getCountries = countries.map(newCount =>

      <Countries key={newCount.id}
                 country={newCount.name}
                 onClick={() => setSelectedCountry(newCount.alpha3Code)}
      />
  );

  const click = e => {
      e.preventDefault();

  }

  return (
    <div className="App">
      <div className='title'>Countries</div>
        <div className="allBlock">
            <div>
                <ul className="countries" onClick={click}>
                    {getCountries}
                </ul>
            </div>
            <div className="aboutCount">
              <Selected alpha3Code={selectedCountry}/>`
            </div>

        </div>

    </div>
  );
}

export default App;
