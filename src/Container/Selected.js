import React, {useEffect, useState} from 'react';
import './Selected.css';
import axios from "axios";


const Selected = ({alpha3Code}) => {

    const [aboutCountry, setAboutCountry] = useState(null);

    useEffect(() => {

        const fetchData = async () => {

            if (alpha3Code !== null) {
                const res = await axios.get('https://restcountries.eu/rest/v2/alpha/' + alpha3Code);

                const about = res.data

                setAboutCountry(about);
            }

        };

        fetchData().catch(console.error)
    }, [alpha3Code]);


    return  aboutCountry && (

        <div className="about">
            <p>Name : {aboutCountry.name}</p>
            <p>Borders : {aboutCountry.borders}</p>
            <p>Capital : {aboutCountry.capital}</p>

        </div>

    );
};

export default Selected;