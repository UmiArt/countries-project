import React from 'react';
import './Countries.css';

const Countries = props => {
    return (
        <li>
            <a href='countryLink' onClick={props.onClick}>
                {props.country}
            </a>
        </li>

    );
};

export default Countries;